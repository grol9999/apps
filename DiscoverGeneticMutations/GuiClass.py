import pandas
import json

from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QPushButton, QTextEdit
from reportlab.graphics.charts.legends import Legend
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.lib.validators import Auto
from reportlab.graphics.charts.piecharts import Pie
from reportlab.graphics.shapes import Drawing, String

from DetermineMutation import process_dna_data, generic_count_occurr_stats
from FileNormalizer import file_normalizer_ancestry, file_normalizer_23, clean_23me_csv_file, clean_anc_csv_file, \
    data_source_name_23, data_source_name_anc
from ConstantsGui import *


class MainGui(QMainWindow):
    def __init__(self):
        super().__init__()

        self.title = main_win_title
        self.left = main_window_pos_left
        self.top = main_window_pos_top
        self.width = main_window_width
        self.height = main_window_height

        self.btn_23me = QPushButton("Select File - 23Me", self)
        self.btn_anc = QPushButton("Select File - Ancestry", self)
        self.btn_rsid_lookup = QPushButton("Select File - rsid lookup", self)
        self.btn_output = QPushButton("Report Save location", self)
        self.btn_clear = QPushButton("Reset configuration!", self)
        self.btn_gen = QPushButton("Generate Report", self)
        self.text_edit = QTextEdit(self)
        self.btn_chkbox_workaround = QPushButton("Enable/Disable extra stats", self)

        # damn checkbox, should've worked...
        # layout = QVBoxLayout()
        # self.chk_box_gen_stats = QCheckBox("Extra stats in report")
        # layout.addWidget(self.chk_box_gen_stats)
        # self.setLayout(layout)

        self.f23me = None
        self.fanc = None
        self.fout = None

        self.rsid_file_name = None
        self.rsid_lookup_list = []
        self.results = []
        self.stats_23 = None
        self.stats_anc = None

        self.twm_df_obj = None
        self.anc_df_obj = None
        self.button_check_gen_extra_stats = False

        self.init_ui()

    def init_ui(self):
        # button 1: select 23me file
        self.btn_23me.setGeometry(50, 50, 180, 25)
        self.btn_23me.clicked.connect(self.dialog_file_23)

        # button 2: select Ancestry file
        self.btn_anc.setGeometry(50, 100, 180, 25)
        self.btn_anc.clicked.connect(self.dialog_file_anc)

        # button 3: file with search criteria
        self.btn_rsid_lookup.setGeometry(50, 150, 180, 25)
        self.btn_rsid_lookup.clicked.connect(self.rsid_file_select_function)

        # button 4: select output directory
        self.btn_output.setGeometry(50, 200, 180, 25)
        self.btn_output.clicked.connect(self.dialog_file_out)

        # button 5: reset this class object's properties
        self.btn_clear.setGeometry(50, 250, 180, 25)
        self.btn_clear.clicked.connect(self.reset_config)

        # button 6: generate report based on "self.results"
        self.btn_gen.setGeometry(50, 400, 180, 50)
        self.btn_gen.clicked.connect(self.generate_report)

        # chk box
        self.btn_chkbox_workaround.setGeometry(50, 450, 180, 50)
        self.btn_chkbox_workaround.clicked.connect(self.click_checkbox)

        self.text_edit.setGeometry(250, 50, 450, 400)

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar().showMessage(main_win_status_bar_msg)
        self.show()

    def dialog_file_23(self):
        file_name = QFileDialog.getOpenFileName(self, "Select File - 23Me", "/")
        if file_name[0]:
            self.f23me = file_name[0]
            self.text_edit.append("Selected 23me file is: ")
            self.text_edit.append(str(file_name[0]))
            self.text_edit.append("-"*50)

    def dialog_file_anc(self):
        file_name = QFileDialog.getOpenFileName(self, "Select File - Ancestry", "/")
        if file_name[0]:
            self.fanc = file_name[0]
            self.text_edit.append("Selected Ancestry file is: ")
            self.text_edit.append(str(file_name[0]))
            self.text_edit.append("-"*50)

    def rsid_file_select_function(self):
        file_name = QFileDialog.getOpenFileName(self, "Select File - RSID", "/")
        if file_name[0]:
            self.rsid_file_name = file_name[0]
            self.text_edit.append("Selected RSID file is: ")
            self.text_edit.append(str(file_name[0]))
            self.text_edit.append("-" * 50)

    def dialog_file_out(self):
        file_name = QFileDialog.getExistingDirectory(self, "Select Directory", "/")
        if file_name:
            self.fout = file_name
            self.text_edit.append("Selected output folder: ")
            self.text_edit.append(file_name)
            self.text_edit.append("-" * 50)

    def reset_config(self, clear_me=False):
        if clear_me:
            self.text_edit.clear()

        self.f23me = None
        self.fanc = None
        self.fout = None
        self.rsid_file_name = None
        self.rsid_lookup_list = []
        self.results = []
        self.twm_df_obj = None
        self.anc_df_obj = None

    def click_checkbox(self):
        if self.button_check_gen_extra_stats is False:
            self.button_check_gen_extra_stats = True
            self.text_edit.append('Enabled extra stats in report!')
        else:
            self.button_check_gen_extra_stats = False
            self.text_edit.append('Disabled extra stats in report!')

    def generate_report(self):
        # print(self.fout)
        self.rsid_lookup_list = []
        self.results = []
        self.stats_23 = None
        self.stats_anc = None
        self.text_edit.clear()

        normalized_at_least_one = False
        if self.f23me:
            file_normalizer_23(self.f23me)
            normalized_at_least_one = True

        if self.fanc:
            file_normalizer_ancestry(self.fanc)
            normalized_at_least_one = True

        if normalized_at_least_one:
            self.process_cleaned_files()

    def generate_rsid_list(self):
        rsid_read_file = pandas.read_csv(self.rsid_file_name)
        for idx, row in rsid_read_file.iterrows():
            self.rsid_lookup_list.append(row['SNP'])

    def process_cleaned_files(self):
        if self.f23me:
            self.twm_df_obj = pandas.read_csv(clean_23me_csv_file, low_memory=False)
            # self.text_edit.append("Processed data sources, saved cleaned csv 23Me, almost there...")
            if self.btn_chkbox_workaround:
                self.stats_23 = generic_count_occurr_stats(self.twm_df_obj)
                # self.text_edit.append("Generated statistics, please wait a little longer...")
        if self.fanc:
            self.anc_df_obj = pandas.read_csv(clean_anc_csv_file, low_memory=False)
            # self.text_edit.append("Processed data sources, saved cleaned csv 23Me, almost there...")
            if self.btn_chkbox_workaround:
                self.stats_anc = generic_count_occurr_stats(self.anc_df_obj)
                # self.text_edit.append("Generated statistics, please wait a little longer...")
        # with this function call, a list of RSID to search for is created by reading the provided CSV file
        self.generate_rsid_list()

        if len(self.rsid_lookup_list) == 0:
            self.text_edit.append("Rsid lookup list is empty!! Can't Continue!")
            self.text_edit.append("Reset Config!")
            self.reset_config(clear_me=True)
            return

        for x in self.rsid_lookup_list:
            if self.f23me:
                for element_df in process_dna_data(self.twm_df_obj, x, data_source_name_23):
                    self.results.append(element_df)
            if self.fanc:
                for element_df in process_dna_data(self.anc_df_obj, x, data_source_name_anc):
                    self.results.append(element_df)

        self.generate_pdf()

    def generate_pdf(self):
        # TODO placeholder value to skip checking, config file is actually not used right now
        my_config_data = True
        # try:
        #     json_config_file = open('pdf.conf', 'r')
        #     my_config_data = json.load(json_config_file)
        # except Exception as err:
        #     self.text_edit.append("An error occurred while trying to load the config file:")
        #     self.text_edit.append(str(err))

        if my_config_data:
            try:
                self.build_my_canvas()
            except Exception as err:
                self.reset_config()
                self.text_edit.append("An error occurred while trying to generate the pdf file:")
                self.text_edit.append(str(err))
                self.text_edit.append("")
                self.text_edit.append("Configuration was reset!")

        else:
            self.reset_config(clear_me=True)
            self.text_edit.append("Can't generate report, incorrect configuration file.")
            self.text_edit.append("Configuration was reset!")

        # print(self.fout)
        self.text_edit.append("Done, you can access your report here: " + self.fout + "report.pdf")

    def build_my_canvas(self):
        # color table here -> https://www.rapidtables.com/web/color/index.html
        local_canvas_obj = SimpleDocTemplate(f"{self.fout}report.pdf",
                                             pagesize=letter,
                                             rightMargin=72,
                                             leftMargin=72,
                                             topMargin=72,
                                             bottomMargin=18)
        data_to_add = []
        my_style = getSampleStyleSheet()

        data_to_add.append(Paragraph(f"<font color=#008080> Genetics Report </font>", my_style["h1"]))
        data_to_add.append(Spacer(width=0, height=30))

        if self.button_check_gen_extra_stats:
            if self.stats_23:
                data_to_add.append(self.pie_chart_with_legend(self.stats_23, '23Me Data Source'))
            if self.stats_anc:
                data_to_add.append(self.pie_chart_with_legend(self.stats_anc, 'Ancestry Data Source'))

        t23_me_data = []
        anc_data = []
        t23_me_data.append(Paragraph(f"<font color=#008080> 23Me Report data </font>", my_style["h3"]))
        anc_data.append(Paragraph(f"<font color=#008080> Ancestry Report data </font>", my_style["h3"]))

        for el in self.results:
            determined_color_bg = '#87CEFA'
            det_font_color = '#000000'
            mutation_status = 'Unknown'
            mutation_key_name = 'Mutation'

            if el['Missing 1 genotype']:
                mutation_status = 'missing information'
            elif '-' in el['genotypes']:
                mutation_status = 'unknown information'
            else:
                if el['Mutation level'] == 'Red':
                    determined_color_bg = '#B22222'
                    mutation_status = 'bad mutation'
                elif el['Mutation level'] == 'Yellow':
                    determined_color_bg = '#FFFF00'
                    mutation_status = 'not too bad'
                elif el['Mutation level'] == 'Green':
                    determined_color_bg = '#228B22'
                    mutation_status = 'you\'re awesome'
                elif el['Mutation level'] == 'Insertion':
                    determined_color_bg = '#CD853F'
                    mutation_status = 'inserted genotypes'
                elif el['Mutation level'] == 'Deletion':
                    determined_color_bg = '#E6BE8A'
                    mutation_status = 'deleted genotypes'

            # if the genotype pairs are missing , it will still add - str(el['genotypes'])
            # without actually checking that the list is empty, and possibly write another custom message
            p_add = f"<font color=#B22222> {mutation_key_name} for {el['RSID LookedUp']} with genotype pairs " \
                f"{str(el['genotypes'])}:</font> <font color={det_font_color} " \
                f"backcolor={determined_color_bg}> {mutation_status}</font> "
            if el['DataSource'] == data_source_name_23:
                t23_me_data.append(Paragraph(p_add, my_style["df"]))
            elif el['DataSource'] == data_source_name_anc:
                anc_data.append(Paragraph(p_add, my_style["df"]))
            else:
                anc_data.append(Paragraph(p_add, my_style["df"]))

        data_to_add = data_to_add + t23_me_data
        data_to_add = data_to_add + anc_data
        local_canvas_obj.build(data_to_add)

    def add_legend(self, draw_obj, chart, data):
        legend = Legend()
        legend.alignment = 'right'
        legend.x = 10
        legend.y = 70
        legend.colorNamePairs = Auto(obj=chart)
        draw_obj.add(legend)

    def pie_chart_with_legend(self, p_data, stats_name):
        data = []
        for k, v in p_data[1].items():
            data.append(v)
        drawing = Drawing(width=400, height=200)
        my_title = String(250, 70, 'Statistics chart ' + stats_name, fontSize=14)
        pie = Pie()
        pie.sideLabels = True
        pie.x = 120
        pie.y = 90
        pie.data = data
        pie.labels = [l_char for l_char in 'ACGTID-U']
        pie.slices.strokeWidth = 0.5
        drawing.add(my_title)
        drawing.add(pie)
        self.add_legend(drawing, pie, data)
        return drawing
