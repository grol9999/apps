regex_match = r"\S+"

header_csv = ["SNP", "Chromozome", "Id Marker", "GenoType1", "GenoType2"]

# output_file_names = {"ancestry": "./cleaned_csv_files/ancestry_normalized.csv",
#                      "23me": "./cleaned_csv_files/23me_normalized.csv"}

rsid_str = "rsid"
chromo_str = "chromosome"

clean_23me_csv_file = r'./23me_normalized.csv'
clean_anc_csv_file = r'./ancestry_normalized.csv'

occurrences_matrix = {
    'A': 0,
    'C': 0,
    'G': 0,
    'T': 0,
    'I': 0,
    'D': 0,
    '-': 0,
    'Missing': 0
}

data_source_name_23 = '23Me'
data_source_name_anc = 'Ancestry'

pdf_conf_file_location = r'./pdf.conf'
