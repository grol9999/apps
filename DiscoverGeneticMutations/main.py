import sys

from PyQt5.QtWidgets import QApplication

from GuiClass import MainGui

if __name__ == '__main__':
    application_obj = QApplication([])
    execute_app = MainGui()
    sys.exit(application_obj.exec_())
