import copy

from ConstantsFiles import occurrences_matrix


def det_genotype_combination(geno_types, wanted_snp, file_type):
    good_geno = 0
    bad_geno = 0
    insert_geno = 0
    delete_geno = 0
    unkn_geno = 0
    missing_genome = False
    mutation_level = None

    for geno_x in geno_types:
        geno_x = str(geno_x)

        if geno_x.upper() == 'A':
            good_geno += 1

        elif geno_x.upper() == 'C':
            good_geno += 1

        elif geno_x.upper() == 'G':
            bad_geno += 1

        elif geno_x.upper() == 'T':
            bad_geno += 1

        elif geno_x.upper() == 'I':
            insert_geno += 1

        elif geno_x.upper() == 'D':
            delete_geno += 1

        elif geno_x.upper() == '-':
            unkn_geno += 1

    mis_gen_calculus = good_geno + bad_geno + insert_geno + delete_geno + unkn_geno
    if mis_gen_calculus != 2:
        missing_genome = True
    else:
        if good_geno == 2:
            mutation_level = 'Green'
        elif bad_geno == 2:
            mutation_level = 'Red'
        elif (good_geno+bad_geno) == 2:
            mutation_level = 'Yellow'

        # does the case where "-" is combined with anything else? if yes this should remain like this
        # else , the verification should be: insert_geno == 2
        elif insert_geno >= 1:
            mutation_level = 'Insertion'
        elif delete_geno >= 1:
            mutation_level = 'Deletion'
        elif unkn_geno >= 1:
            mutation_level = 'Unknown'
    final_info = {"DataSource": file_type,
                  "genotypes": geno_types,
                  "Mutation level": mutation_level,
                  "Missing 1 genotype": missing_genome,
                  "RSID LookedUp": wanted_snp}

    return final_info


def process_dna_data(panda_object=None, wanted_snp=None, file_type=''):
    list_of_genotypes = []
    for row_row in panda_object.to_records():
        if row_row[1] == wanted_snp:
            try:
                list_of_genotypes.append(row_row[4])
            except Exception as err:
                print(err)

            try:
                list_of_genotypes.append(row_row[5])
            except Exception as err:
                print(err)

    yield det_genotype_combination(list_of_genotypes, wanted_snp, file_type)


def generic_count_occurr_stats(panda_object):
    counter_stat = copy.deepcopy(occurrences_matrix)
    percent_stat = copy.deepcopy(occurrences_matrix)

    for row_row in panda_object.to_records():
        try:
            counter_stat[(row_row[4])] += 1
        except Exception as err:
            pass
            # print(err)

        try:
            counter_stat[(row_row[5])] += 1
        except Exception as err:
            # print(err)
            pass

    total_val = 0
    for values in counter_stat.values():
        total_val += values

    for k, v in counter_stat.items():
        try:
            percentile_stat = (100 * v)/total_val
            percent_stat[k] = round(percentile_stat, 3)
        except Exception as err:
            pass
            # print(err)

    return counter_stat, percent_stat


