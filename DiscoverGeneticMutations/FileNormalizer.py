import re
from ConstantsFiles import *


def file_normalizer_ancestry(file_name=''):
    opened_file = open(file_name, "r")
    ancestry_normalized = open(clean_anc_csv_file, 'w')

    for line_read_from_file in opened_file:
        if '#' in line_read_from_file:
            continue

        matcher_line = re.findall(regex_match, line_read_from_file)

        if len(matcher_line) > 1:
            if matcher_line[0] == rsid_str:
                list_to_row = make_list_to_csv_row(header_csv)
            else:
                list_to_row = make_list_to_csv_row(matcher_line)

            ancestry_normalized.write(list_to_row)

    ancestry_normalized.close()
    opened_file.close()


def file_normalizer_23(file_name=''):
    opened_file = open(file_name, "r")
    ancestry_normalized = open(clean_23me_csv_file, 'w')

    special_case_header = False
    for line_read_from_file in opened_file:
        if '#' in line_read_from_file:
            if (rsid_str and chromo_str) in line_read_from_file:
                special_case_header = True
            else:
                continue

        matcher_line = re.findall(regex_match, line_read_from_file)

        if len(matcher_line) > 1 or special_case_header:
            if special_case_header:
                list_to_row = make_list_to_csv_row(header_csv)
                special_case_header = False
            else:
                if len(matcher_line) == 4:
                    if len(matcher_line[3]) == 2:
                        tmp_1 = matcher_line[3][1:]
                        tmp_2 = matcher_line[3][:1]
                        matcher_line[3] = tmp_1
                        matcher_line.append(tmp_2)
                list_to_row = make_list_to_csv_row(matcher_line)

            ancestry_normalized.write(list_to_row)

    ancestry_normalized.close()
    opened_file.close()


def make_list_to_csv_row(my_list=None):
    row_to_write = ''
    for i in range(0, len(my_list)):

        if i == (len(my_list)-1):
            row_to_write = row_to_write + my_list[i] + '\n'
        else:
            row_to_write = row_to_write + my_list[i] + ','

    return row_to_write

