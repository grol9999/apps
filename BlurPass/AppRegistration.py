from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
import hashlib
import secrets

import Utils
from SqlWrapper import SqlHighWrap


class RegisterView(Screen):
    def __init__(self, **kw):
        super(RegisterView, self).__init__(**kw)

        self.box_layout = BoxLayout(orientation='vertical', padding=[25], size_hint=(1, 1))
        self.screen_container = GridLayout(cols=2, rows=5, spacing=5, padding=[15, 15, 15, 15], size_hint=(1, 0.7))

        self.label_user = Label(text="Username", size_hint=(0.4, 0.2))
        self.label_pass = Label(text="Password", size_hint=(0.4, 0.2))
        label_imp_warn_1 = Label(text="Important notice: the application does not store your password! "
                                      "The password is used to encrypt your saved credentials with an "
                                      "additional random seed, by doing some crypto 'magic'. "
                                      "If you lose your password, your saved data can't be recovered",
                                 size_hint=(1, 0.9), bold=True, color=(0, 1, 0, 1))
        label_imp_warn_1.bind(width=lambda *x: label_imp_warn_1.setter('text_size')(label_imp_warn_1,
                                                                                    (label_imp_warn_1.width, None)))

        self.save_button = Button(text="Save", font_size=14, size_hint=(0.8, 0.3), halign="left",
                                  background_color=(50, 100, 0, 0.5))
        self.cancel_button = Button(text="Cancel", font_size=14, size_hint=(0.8, 0.3), halign="right",
                                    background_color=(50, 100, 0, 0.5))
        self.save_button.bind(on_press=self._save_new_user)
        self.cancel_button.bind(on_press=self._back_to_login)
        self.input_user = TextInput(halign="left", size_hint=(0.4, 0.3))
        self.input_pass = TextInput(halign="left", size_hint=(0.4, 0.3), password=True)

        self.invisible_label_1 = Label(size_hint=(0, 0))
        self.invisible_label_2 = Label(size_hint=(0, 0))
        self.screen_container.add_widget(self.invisible_label_1)
        self.screen_container.add_widget(self.invisible_label_2)

        self.screen_container.add_widget(self.label_user)
        self.screen_container.add_widget(self.input_user)

        self.screen_container.add_widget(self.label_pass)
        self.screen_container.add_widget(self.input_pass)

        self.add_widget(self.box_layout)
        self.warn_text = Label(text="WARNING! Only 1 user can exist at a time! If you create a new user, "
                                    "existing user and associated data will be erased! WARNING!",
                               size_hint_min_x=1, color=(1, 0, 0, 1))
        self.warn_text.bind(width=lambda *x: self.warn_text.setter('text_size')(self.warn_text,
                                                                                (self.warn_text.width, None)))
        self.box_layout.add_widget(self.warn_text)
        self.box_layout.add_widget(self.screen_container)
        self.box_layout.add_widget(label_imp_warn_1)

        self.screen_container.add_widget(self.save_button)
        self.screen_container.add_widget(self.cancel_button)
        self.err_label = Label(text='', color=(1, 0, 1, 1), size_hint=(1, 0.4))
        self.screen_container.add_widget(self.err_label)

    def _save_new_user(self, obj_instance):
        usr_chk = self.input_user.text is None or self.input_user.text == '' or \
                  self.input_pass.text is None or self.input_pass.text == ''
        if usr_chk:
            self.err_label.text = "Username or password cannot be empty!"
            return False
        # TODO verify user already exist! in multi user , change flag to - clean_init=False
        sql_wrap_obj = SqlHighWrap(clean_init=True)
        sql_wrap_obj.create_table(self.input_user.text, "(PassHash string, UserName string)")

        salt = secrets.token_hex(16).encode('utf-8')
        d_salt = salt.decode('utf8')
        d_salt = 'Q' + d_salt
        Utils.salt_tn = d_salt

        sql_wrap_obj.create_table(d_salt, "(Data string)")
        p_enc = str(self.input_pass.text).encode('utf-8')

        hashed_pass = d_salt + "_" + hashlib.sha512(salt+p_enc).hexdigest()

        try:
            sql_wrap_obj.add_user(self.input_user.text, hashed_pass)
            self._back_to_login(1)
        except Exception as err:
            print(err)

    def _back_to_login(self, obj_instance):
        self.parent.get_screen('LoginScreen').do_not_remember_user()
        self.parent.current = 'LoginScreen'

    def on_enter(self, *args):
        self.err_label.text = ''
        self.input_pass.text = ''
        self.input_user.text = ''
