import hashlib

from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput

from SqlWrapper import SqlHighWrap
import Utils


class AppLoginScreen(Screen):
    def __init__(self, **kwargs):
        super(AppLoginScreen, self).__init__(**kwargs)

        self.screen_container = BoxLayout(orientation='vertical', padding=[1])
        self.add_widget(self.screen_container)  # add the box widget to the "root" / "self" /
        self.image_logo = Image(source="./assets/main.png")
        self.placeholder_1 = Label(text="P1", size_hint_y=0.2, color=(1, 1, 1, 0))
        self.placeholder_2 = Label(text="P2", size_hint_y=0.1, color=(1, 1, 1, 0))
        self.register_btn = Button(text="Register", font_size=15, size_hint_y=0.2, size_hint_x=0.5, halign="center",
                                   color=(1, 1, 1, 1), background_color=(55, 1, 1, 0.5))
        self.register_btn.bind(on_press=self._register_user)
        self.box_register = BoxLayout(orientation='horizontal', padding=[5], size_hint=(1, 0.9))
        self.placeholder_reg_1 = Label(text="R1", color=(1, 1, 1, 0))
        self.placeholder_reg_2 = Label(text="R2", color=(1, 1, 1, 0))
        self.box_register.add_widget(self.placeholder_reg_1)
        self.box_register.add_widget(self.register_btn)
        self.box_register.add_widget(self.placeholder_reg_2)
        self.placeholder_1_1 = Label(text="P11", size_hint_x=0.2, color=(1, 1, 1, 0))
        self.placeholder_1_2 = Label(text="P12", size_hint_x=0.2, color=(1, 1, 1, 0))
        self.placeholder_2_1 = Label(text="P21", size_hint_x=0.2, color=(1, 1, 1, 0))
        self.placeholder_2_2 = Label(text="P22", size_hint_x=0.2, color=(1, 1, 1, 0))
        self.placeholder_f = Label(text="", size_hint_y=0.3, color=(1, 0, 0, 1))

        self.box_user = BoxLayout(orientation='horizontal', padding=[5], size_hint_y=0.2)
        self.box_pass = BoxLayout(orientation='horizontal', padding=[5], size_hint_y=0.2)
        self.user_label = Label(text="Username: ", halign="left", font_size=17, color=(0, 25, 0, 1), bold=False, size_hint=(0.3, 0.8))
        self.pass_label = Label(text="Password: ", halign="left", font_size=17, color=(0, 25, 0, 1), bold=False, size_hint=(0.3, 0.8))
        self.user_input = TextInput(halign="left", size_hint=(0.3, 0.8))
        self.pass_input = TextInput(halign="left", password=True, size_hint_y=0.3, size_hint=(0.3, 0.8))

        self.box_log_btn = BoxLayout(orientation='horizontal', padding=[1], size_hint_y=0.4)
        self.next_page = Button(text="Login", font_size=18, size_hint_y=0.4, size_hint_x=0.8, color=(125, 55, 125, 1),
                                background_color=(1, 1, 1, 0.6))
        self.next_page.bind(on_press=self._validate_login)
        self.placeholder_3 = Label(text="P3", size_hint_y=0.3, color=(1, 1, 1, 0))
        self.placeholder_4 = Label(text="P4", size_hint_y=0.3, color=(1, 1, 1, 0))
        self.screen_container.add_widget(self.image_logo)
        self.screen_container.add_widget(self.placeholder_1)

        self.screen_container.add_widget(self.box_user)
        self.screen_container.add_widget(self.box_pass)

        self.box_user.add_widget(self.placeholder_1_1)
        self.box_user.add_widget(self.user_label)
        self.box_user.add_widget(self.user_input)
        self.box_user.add_widget(self.placeholder_1_2)

        self.box_pass.add_widget(self.placeholder_2_1)
        self.box_pass.add_widget(self.pass_label)
        self.box_pass.add_widget(self.pass_input)
        self.box_pass.add_widget(self.placeholder_2_2)

        self.screen_container.add_widget(self.box_log_btn)
        self.box_log_btn.add_widget(self.placeholder_3)
        self.box_log_btn.add_widget(self.next_page)
        self.box_log_btn.add_widget(self.placeholder_4)
        self.screen_container.add_widget(self.placeholder_2)
        self.screen_container.add_widget(self.box_register)
        self.screen_container.add_widget(self.placeholder_f)

    def _validate_login(self, obj_instance):
        sql_wrap_obj = SqlHighWrap(clean_init=False)
        sql_wrap_obj.sql_chg_dbname("./BlurPass.db")
        sql_res = sql_wrap_obj.shw_rd_table(sq_tbl_name=self.user_input.text)
        if sql_res:
            p_enc = str(self.pass_input.text).encode('utf-8')
            strip_r = sql_res[0][0].split('_')
            salt_ex = strip_r[0][1:]
            hashed_ex = strip_r[1]
            # print(salt_ex)
            salt_ex = salt_ex.encode('utf-8')

            chk_hash = hashlib.sha512(salt_ex+p_enc).hexdigest()
            if chk_hash == hashed_ex:
                Utils.current_decryption_key = [self.pass_input.text, salt_ex]
                Utils.data_s = Utils.obtain_key()
                self.parent.get_screen('ListView').populate_list()
                self.parent.current = 'ListView'
            else:
                self.placeholder_f.text = 'Invalid Username or Password'
                # print("Invalid Pass!")
                pass
        else:
            self.placeholder_f.text = 'Invalid Username or Password'
            # print("No such username!")
            pass

    def _register_user(self, obj_instance):
        self.placeholder_f.text = ''
        self.parent.current = 'RegisterView'

    def do_not_remember_user(self):
        self.placeholder_f.text = ''
        self.user_input.text = ''
        self.pass_input.text = ''
