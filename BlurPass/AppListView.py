import base64

from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput

import Utils
from SqlWrapper import SqlHighWrap


class AppSearchList(Screen):
    def __init__(self, **kw):
        super(AppSearchList, self).__init__(**kw)

        self.list_btn_ids = {}

        self.main_box_layout = BoxLayout(orientation="vertical", padding=[5], size_hint=(1, 1))
        self.menu_box_layout = BoxLayout(orientation="horizontal", padding=[5], size_hint=(1, 0.5))
        self.menu_box_layout.add_widget(Label(text="Search Username", size_hint=(0.3, 0.6), valign="bottom",
                                              halign="left", color=(0, 1, 0, 1), font_size=16))
        self.logout_btn = Button(text='Logout', size_hint=(0.3, 0.6), font_size=16, color=(1, 1, 1, 1),
                                 background_color=(55, 1, 1, 0.5))
        self.logout_btn.bind(on_press=self._logout_app)
        self.menu_box_layout.add_widget(self.logout_btn)

        self.main_box_layout.add_widget(self.menu_box_layout)

        self.menu_box_layout_2 = BoxLayout(orientation='horizontal', padding=[5], size_hint=(1, 0.5))
        self.main_box_layout.add_widget(self.menu_box_layout_2)

        # self.input_search = TextInput(halign="left", size_hint=(0.45, 0.6))
        self.input_search = Label(text="Search is disabled", halign="left", size_hint=(0.45, 0.6), color=(0, 1, 0, 1))
        self.menu_box_layout_2.add_widget(self.input_search)
        self.new_entry_btn = Button(text="Add New", font_size=16, size_hint=(0.45, 0.6), color=(125, 55, 125, 1),
                                    background_color=(1, 1, 1, 0.6))
        self.new_entry_btn.bind(on_press=self._add_new_entry)
        self.menu_box_layout_2.add_widget(self.new_entry_btn)
        self.main_box_layout.add_widget(Label(text="List of credentials:", size_hint=(1, 0.3)))
        self.add_widget(self.main_box_layout)

        self.layout = GridLayout(cols=1, spacing=6, size_hint_y=None)

        # there should be something there in the list, otherwise the scroll won't be enabled
        self.layout.bind(minimum_height=self.layout.setter('height'))

        self.root = ScrollView(size_hint=(1, None), size=(Window.width, Window.height))
        self.root.add_widget(self.layout)
        self.main_box_layout.add_widget(self.root)

    def _add_new_entry(self, obj_instance):
        self.parent.current = 'NewEntryView'

    def populate_list(self):
        Utils.data_detail = {}
        self.list_btn_ids = {}
        sql_wrap_obj = SqlHighWrap(clean_init=False)
        sql_wrap_obj.sql_chg_dbname("./BlurPass.db")

        try:
            salt = Utils.current_decryption_key[1]
            salt = 'Q'+salt.decode()
            sql_res = sql_wrap_obj.shw_rd_table(sq_tbl_name=salt)

            self.layout.clear_widgets()
            i = 0
            for enc_data in sql_res:
                data_proc = enc_data[0]
                data_proc_enc = enc_data[0]
                data_proc = base64.b64decode(data_proc)
                data_proc = Utils.data_s.decrypt(data_proc)
                data_proc = data_proc.decode()
                data_proc = list(map(str.strip, data_proc.strip('][').replace("'", '').split(',')))

                label = str("Username: " + data_proc[0])+" & "+"Server addr:"+str(data_proc[2])

                Utils.data_detail[label] = {}
                Utils.data_detail[label]['btn'] = Button(text=label, size_hint_y=None, height=40, color=(0, 1, 0, 1),
                                                         background_color=(128, 128, 128, 0.2))
                Utils.data_detail[label]['data'] = data_proc
                Utils.data_detail[label]['enc'] = data_proc_enc

                Utils.data_detail[label]['btn'].bind(on_press=self.get_detailed_view)

                self.layout.add_widget(Utils.data_detail[label]['btn'])
                i += 1
        except Exception as err:
            # print(err)
            pass

    def get_detailed_view(self, obj_instance):
        for k, v in Utils.data_detail.items():
            if k == obj_instance.text:
                self.parent.get_screen('DetailView').get_detail_view(Utils.data_detail[k]['data'])
                break
        self.parent.current = 'DetailView'

    def _logout_app(self, obj_instance):
        Utils.current_decryption_key = []
        Utils.data_s = None
        Utils.data_detail = {}
        self.parent.get_screen('LoginScreen').do_not_remember_user()
        self.parent.current = 'LoginScreen'
