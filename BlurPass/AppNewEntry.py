import base64

from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput


import Utils
from SqlWrapper import SqlHighWrap


class AppNewEntry(Screen):
    def __init__(self, **kw):
        super(AppNewEntry, self).__init__(**kw)

        self.screen_container = GridLayout(cols=2, rows=8, spacing=[55, 55], padding=25, size_hint=(1, 1))

        label_user = Label(text="Username")
        label_pass = Label(text="Password")
        label_server = Label(text="Server")
        label_sq = Label(text="Secret Quest.")
        label_notes = Label(text="Notes")

        self.input_user = TextInput(halign="left", size_hint=(0.8, 0.4))
        self.input_pass = TextInput(halign="left", size_hint=(0.8, 0.4))
        self.input_server = TextInput(halign="left", size_hint=(0.8, 0.4))
        self.input_sq = TextInput(halign="left", size_hint=(0.8, 0.5))
        self.input_notes = TextInput(halign="left", size_hint=(0.8, 0.8))

        self.ph_1_message = Label(halign="left", size_hint=(0.8, 0.8), color=(0, 1, 0, 1))
        self.ph_1_message.bind(width=lambda *x: self.ph_1_message.setter('text_size')(self.ph_1_message,
                                                                                      (self.ph_1_message.width, None)))
        self.ph_2_message = Label(halign="left", size_hint=(0.8, 0.8))
        self.screen_container.add_widget(self.ph_1_message)
        self.screen_container.add_widget(self.ph_2_message)

        self.screen_container.add_widget(label_user)
        self.screen_container.add_widget(self.input_user)

        self.screen_container.add_widget(label_pass)
        self.screen_container.add_widget(self.input_pass)

        self.screen_container.add_widget(label_server)
        self.screen_container.add_widget(self.input_server)

        self.screen_container.add_widget(label_sq)
        self.screen_container.add_widget(self.input_sq)

        self.screen_container.add_widget(label_notes)
        self.screen_container.add_widget(self.input_notes)

        self.add_widget(self.screen_container)

        self.save_button = Button(text="Save", font_size=15, size_hint=(0.8, 1), halign="center",
                                  background_color=(50, 100, 0, 0.3))
        self.cancel_button = Button(text="Cancel", font_size=15, size_hint=(0.8, 1), halign="center",
                                    background_color=(50, 100, 0, 0.3))
        self.save_button.bind(on_press=self._save_new_entry)
        self.cancel_button.bind(on_press=self._cancel_save_entry)
        self.screen_container.add_widget(self.save_button)
        self.screen_container.add_widget(self.cancel_button)

        for i in range(0, 2):
            placeholder = Label()
            self.screen_container.add_widget(placeholder)

    def _save_new_entry(self, obj_instance):
        salt = Utils.current_decryption_key[1]
        f_key = Utils.data_s

        # hack! Bwahaha!!!
        self.input_pass.text = self.input_pass.text.replace("'", "&915;")

        # let's continue.
        data_save = [self.input_user.text,
                     self.input_pass.text,
                     self.input_server.text,
                     self.input_sq.text,
                     self.input_notes.text]
        # print(self.input_pass.text)
        label = str("Username: " + self.input_user.text + " & " + "Server addr:" + self.input_server.text)
        if label in Utils.data_detail.keys():
            # print("KEY EXISTS!")
            self.ph_1_message.text = "User and server already exist in the database!" \
                                     "Try adding another set of credentials."
            return False

        data_save_encrypt = str(data_save).encode()

        Utils.data_s = f_key
        encrypted = f_key.encrypt(data_save_encrypt)

        sql_wrap_obj = SqlHighWrap(clean_init=False)
        sql_wrap_obj.sql_chg_dbname("./BlurPass.db")

        encoded_data = base64.b64encode(encrypted)

        d_salt = 'Q'+salt.decode()
        d_enc_data = encoded_data.decode()

        sql_wrap_obj.save_data(u_hash=d_enc_data, t_name=d_salt)

        self.parent.get_screen('ListView').populate_list()
        self.parent.current = 'ListView'

    def _cancel_save_entry(self, obj_instance):
        self.parent.current = "ListView"

    def on_enter(self):
        self.ph_1_message.text = ''
        self.input_user.text = ''
        self.input_pass.text = ''
        self.input_server.text = ''
        self.input_sq.text = ''
        self.input_notes.text = ''
