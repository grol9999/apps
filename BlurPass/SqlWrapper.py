import os
import sqlite3
from pathlib import Path


class SqlHighWrap:
    def __init__(self, clean_init=False):
        """
        :type clean_init: bool
        """
        # if clean_init is set to True, the cards_db.db file file will be erased
        # and a new DB will be initialised
        self.con_sql = None
        self.cursor = None
        self.write_counter = -1
        self.write_counter_chk = 0
        db_file_chk = Path("./BlurPass.db")

        if db_file_chk.is_file() and clean_init:
            os.remove("./BlurPass.db")
        self.sql_chg_dbname(sql_dbname="./BlurPass.db")

    def _shw_connect(self, sql_dbname=None):
        """
        :type sql_dbname: string
        """
        # modifies self.con_sql and self.cursor, so that they can be used
        # this is a private method, should not be called directly
        if sql_dbname is None:
            self.con_sql = None
            self.cursor = None
        elif isinstance(sql_dbname, str) and len(sql_dbname) > 1:
            self.con_sql = sqlite3.connect(sql_dbname)
            self.cursor = self.con_sql.cursor()

    def sql_chg_dbname(self, sql_dbname=None):
        """
        :type sql_dbname: string
        :return bool
        """
        # the return value says success True or False
        self._shw_connect(sql_dbname)
        if self.con_sql is None or self.cursor is None:
            return False
        return True

    def _shw_cr_table(self, sq_tbl_name, sq_tbl_col):
        """
        :type sq_tbl_col: string
        :type sq_tbl_name: string
        """
        # creates a table
        if self.cursor is None or sq_tbl_name is None or sq_tbl_col is None:
            return False
        elif sq_tbl_name == '' or sq_tbl_col == '':
            return False
        # print("CREATE TABLE IF NOT EXISTS " + sq_tbl_name + " " + sq_tbl_col)
        self.cursor.execute("CREATE TABLE IF NOT EXISTS " + sq_tbl_name + " " + sq_tbl_col)
        self.con_sql.commit()

    # noinspection SqlNoDataSourceInspection,SqlDialectInspection
    def shw_rd_table(self, sq_tbl_name=None, read_type="*"):
        """
        :type read_type: string
        :type sq_tbl_name: string
        """
        # reads from a given table, default is to read everything
        if (sq_tbl_name is None) or (self.cursor is None):
            return False

        # print("SELECT " + read_type + " FROM " + sq_tbl_name)
        s_query = "SELECT name FROM sqlite_master WHERE type='table' AND name='{0}'"
        exit_chk = self.cursor.execute(s_query.format(sq_tbl_name))
        if len(exit_chk.fetchall()) > 0:
            self.cursor.execute("SELECT " + read_type + " FROM " + sq_tbl_name)
        else:
            return False

        return self.cursor.fetchall()

    def shw_rd_filter_table(self, sq_tbl_name=None, read_type="*", where_read=""):
        """
        :type where_read: string
        :type read_type: string
        :type sq_tbl_name: string
        """
        # reads from a given table, default is to read everything
        if (sq_tbl_name is None) or (self.cursor is None):
            return False
        self.cursor.execute("SELECT " + read_type + " FROM " + sq_tbl_name + " WHERE " + where_read)
        return self.cursor.fetchall()

    def shw_ins_row(self, sq_tbl_name=None, num_cols=None, content=None):
        """
        :type content: tuple
        :type num_cols: string
        :type sq_tbl_name: string
        """
        # adds a row into a table
        if (sq_tbl_name is None) or (num_cols is None) or (content is None):
            return False
        # print("INSERT INTO " + sq_tbl_name + " VALUES " + num_cols, content)
        self.cursor.execute("INSERT INTO " + sq_tbl_name + " VALUES " + num_cols, content)

        # do a bulk write, flush data less often, otherwise performance degradation will occur!
        self.flush_data()

    # def shw_upd_row(self, sq_tbl_name=None, set_values=None, criteria=None, multi_criteria=""):
    #     """
    #     :type multi_criteria: str
    #     :type criteria: dict
    #     :type set_values: dict
    #     :type sq_tbl_name: string
    #     """
    #     # if multicriteria is True, the key-value pairs from a dictionary will be split using AND
    #     # set_values must be a dictionary with pair values to update a row
    #     # the criteria must be a dictionary
    #     if (sq_tbl_name is None) or (set_values is None) or (criteria is None):
    #         return False
    #     if not isinstance(set_values, dict) or not isinstance(criteria, dict):
    #         return False
    #
    #     self.cursor.execute("UPDATE " + sq_tbl_name + " SET " + self.dict_to_sql_format(set_values) + " WHERE "
    #                         + self.dict_to_sql_format(criteria, multi_criteria))
    #     self.con_sql.commit()

    # def shw_drp_table(self, sq_tbl_name):
    #     if sq_tbl_name is None:
    #         return False
    #     self.cursor.execute("DROP TABLE IF EXISTS " + sq_tbl_name)
    #     self.con_sql.commit()

    def shw_del_row(self, sq_tbl_name=None, col_name=None, delete_criteria=None):
        if (sq_tbl_name is None) or (delete_criteria is None) or (col_name is None):
            return False
        self.cursor.execute("DELETE FROM " + sq_tbl_name + " WHERE " + col_name + " = (?)", (delete_criteria,))
        self.con_sql.commit()

    def flush_data(self):
        if self.write_counter_chk > self.write_counter:
            self.con_sql.commit()
            self.write_counter_chk = 0
        self.write_counter_chk += 1

    # def dict_to_sql_format(self, my_dict=None, multi_criteria=None):
    #     # transforms a dictionary into a part of a SQL query
    #     # if multi_criteria is used then an OPERATOR is added & the operator MUST be from the pre-defined list
    #     serialized_value = ""
    #     multi_crit_list = ["AND", "OR", "NOT"]
    #     if multi_criteria in multi_crit_list:
    #         sign_use = " " + multi_criteria + " "
    #     else:
    #         sign_use = ", "
    #
    #     last = len(my_dict.items())
    #     counter = 1
    #     for k, v in my_dict.items():
    #         value_str = ""
    #         if isinstance(v, str):
    #             value_str = '"' + str(v) + '"'
    #         elif isinstance(v, bool):
    #             value_str = str(int(v))
    #         elif isinstance(v, int):
    #             value_str = str(v)
    #         elif v is None:
    #             value_str = "0"
    #
    #         if last == 1:
    #             serialized_value = k + " = " + value_str + " "
    #         elif counter < last:
    #             serialized_value = serialized_value + k + " = " + value_str + sign_use
    #         else:
    #             serialized_value = serialized_value + k + " = " + value_str + " "
    #         counter += 1
    #     return serialized_value

    def create_table(self, table_name, table_cols):
        # set the working DB to specific DB
        self.sql_chg_dbname(sql_dbname="./BlurPass.db")

        # create special tables here
        self._shw_cr_table(table_name, table_cols)

    def add_user(self, t_name, u_hash):
        entry_values_insert = (u_hash, t_name)

        try:
            self.shw_ins_row(sq_tbl_name=t_name, num_cols="(?,?)", content=entry_values_insert)
        except Exception as err:
            pass

    def save_data(self, t_name, u_hash):
        try:
            self.cursor.execute("INSERT INTO " + t_name + " VALUES (?)", (u_hash,))
            self.flush_data()
        except Exception as err:
            pass

    def count_entries_from_table(self, t_name, u_hash):
        try:
            sql_res = self.cursor.execute("SELECT COUNT(*) FROM " + t_name)
            return sql_res
        except Exception as err:
            pass
