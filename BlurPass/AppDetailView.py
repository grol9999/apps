from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button

import Utils
from SqlWrapper import SqlHighWrap


class AppDetailView(Screen):
    def __init__(self, **kw):
        super(AppDetailView, self).__init__(**kw)
        screen_container = BoxLayout(orientation='vertical', padding=[1])
        logo = Label(text="Details")
        screen_container.add_widget(logo)

        self.clear_widgets()

        self.screen_container = GridLayout(cols=2, rows=8, spacing=[55, 55], padding=25, size_hint=(1, 1))

        self.label_user = Label(text="Username")
        self.label_pass = Label(text="Password")
        self.label_server = Label(text="Server")
        self.label_sq = Label(text="Secret Quest.")
        self.label_notes = Label(text="Notes")

        self.input_user = Label(text="", halign="left", size_hint=(0.8, 0.4))
        self.input_pass = Label(text="", halign="left", size_hint=(0.8, 0.4))
        self.input_server = Label(text="", halign="left", size_hint=(0.8, 0.4))
        self.input_sq = Label(text="", halign="left", size_hint=(0.8, 0.5))
        self.input_notes = Label(text="", halign="left", size_hint=(0.8, 0.8))

        for i in range(0, 2):
            placeholder = Label()
            self.screen_container.add_widget(placeholder)

        self.screen_container.add_widget(self.label_user)
        self.screen_container.add_widget(self.input_user)

        self.screen_container.add_widget(self.label_pass)
        self.screen_container.add_widget(self.input_pass)

        self.screen_container.add_widget(self.label_server)
        self.screen_container.add_widget(self.input_server)

        self.screen_container.add_widget(self.label_sq)
        self.screen_container.add_widget(self.input_sq)

        self.screen_container.add_widget(self.label_notes)
        self.screen_container.add_widget(self.input_notes)

        self.add_widget(self.screen_container)

        self.delete_button = Button(text="Delete", font_size=15, size_hint=(0.8, 1), halign="center",
                                    background_color=(50, 100, 0, 0.3))
        self.cancel_button = Button(text="Close", font_size=15, size_hint=(0.8, 1), halign="center",
                                    background_color=(50, 100, 0, 0.3))
        self.delete_button.bind(on_press=self._delete_entry)
        self.cancel_button.bind(on_press=self._close_detail_view)
        self.screen_container.add_widget(self.delete_button)
        self.screen_container.add_widget(self.cancel_button)

        for i in range(0, 2):
            placeholder = Label()
            self.screen_container.add_widget(placeholder)

    def _close_detail_view(self, obj_instance):
        self.parent.current = 'ListView'

    def _delete_entry(self, obj_instance):
        for k, v in Utils.data_detail.items():
            chk = str("Username: " + self.input_user.text + " & " + "Server addr:" + self.input_server.text)
            if k == chk:
                sql_wrap_obj = SqlHighWrap(clean_init=False)
                sql_wrap_obj.sql_chg_dbname("./BlurPass.db")

                sql_wrap_obj.shw_del_row(sq_tbl_name='Q'+Utils.current_decryption_key[1].decode(), col_name="Data",
                                         delete_criteria=Utils.data_detail[chk]['enc'])
                break

        # TODO add delete entry from SQL
        self.parent.get_screen('ListView').populate_list()
        self.parent.current = 'ListView'

    def get_detail_view(self, data):
        self.input_user.text = data[0]
        # Reverse hack! Kyahaha!!!
        self.input_pass.text = data[1].replace("&915;", "'")
        self.input_server.text = data[2]
        self.input_sq.text = data[3]
        self.input_notes.text = data[4]
