from kivy.uix.screenmanager import ScreenManager
from kivy.core.window import Window
from kivy.app import App

from AppDetailView import AppDetailView
from AppListView import AppSearchList
from AppLogin import AppLoginScreen
from AppNewEntry import AppNewEntry
from AppRegistration import RegisterView


class GeneratorApp(App):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.screen_manager = ScreenManager()

        app_screens = [AppLoginScreen(name='LoginScreen'),
                       AppSearchList(name='ListView'),
                       AppDetailView(name='DetailView'),
                       RegisterView(name='RegisterView'),
                       AppNewEntry(name='NewEntryView')]

        for app_screen in app_screens:
            self.screen_manager.add_widget(app_screen)

    def build(self):
        Window.clearcolor = (0, 0, 0, 0.5)
        Window.size = (500, 800)

        return self.screen_manager


main_app = GeneratorApp()
main_app.run()
