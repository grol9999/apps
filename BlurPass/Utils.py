import base64

from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes

# key pair
current_decryption_key = []

# data key
data_s = None

data_detail = {}


def obtain_key():
    pass_used_as_key = current_decryption_key[0]
    pass_used_as_key_bytes = pass_used_as_key.encode()
    salt = current_decryption_key[1]
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100,
        backend=default_backend()
    )
    key = base64.urlsafe_b64encode(kdf.derive(pass_used_as_key_bytes))
    return Fernet(key)
