import pandas
import sys
import os
import argparse


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(700, 271)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(20, 20, 131, 21))
        self.pushButton.setObjectName("pushButton")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(170, 20, 400, 192))
        self.listWidget.setObjectName("listWidget")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 600, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.pushButton.clicked.connect(self.selectCsvFiles)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton.setText(_translate("MainWindow", "Select CSV files"))

    def selectCsvFiles(self):
        filenames_csv, _ = QtWidgets.QFileDialog.getOpenFileNames(None, "Select CSV files", "", "CSV Files (*.csv)")
        if filenames_csv:
            self.addMyCsvs(filenames_csv)

    def addMyCsvs(self, csv_list):
        dir_detect = False
        dir_detected = ''
        for x in csv_list:
            x = x.split('/')
            self.listWidget.addItem(x[-1])
            if not dir_detect:
                dir_detect = True
                separator = '/'
                dir_detected = separator.join(x[:-1]) + '/'

        for csv_file_name in csv_list:
            f_in = csv_file_name
            f_out = csv_file_name[:-4] + 'nhtag.csv'
            CsvBeautifier(csv_file=f_in, csv_file_out=f_out)
            self.listWidget.addItem('Done: ' + f_in)


class CsvBeautifier:
    def __init__(self, csv_file=None, csv_file_out=None):
        self._validate_file(csv_file, 'r')
        # self._validate_file(csv_file_out, 'w')

        self.file = csv_file
        self.file_new = csv_file_out

        self.pandas_df = None
        try:
            self.pandas_df = pandas.read_csv(self.file, skiprows=[0])
        except Exception as err_reading_csv:
            print(str(err_reading_csv) + ': '+self.file + '; CSV file not in the expected format')

        self.custom_panda_df = {'Address': [], 'Content': [], 'Status Code': [], 'Indexability': [],
                                'Title 1': [], 'H1-1': [], 'H2-1': [], 'Size (bytes)': []}

        self._run()

    def _gen_panda_custom_df(self):
        extr_column_names = []
        if self.pandas_df is None:
            return False
        for df_keys in self.pandas_df.keys():
            if 'Extractor' in df_keys:
                extr_column_names.append(df_keys)

        for col_to_add in extr_column_names:
            self.custom_panda_df[col_to_add] = []
        for idx, df_row in self.pandas_df.iterrows():

            for keys_l in self.custom_panda_df.keys():
                if keys_l in df_row.keys():
                    x_dst = df_row[keys_l]
                    self.custom_panda_df[keys_l].append(x_dst)

        pandas_df_new = pandas.DataFrame(self.custom_panda_df)
        pandas_df_new.to_csv(self.file_new, index=False)

    def _validate_file(self, file_to_validate=None, read_write=''):
        if file_to_validate is None or not isinstance(file_to_validate, str):
            print("Improper usage")
            sys.exit(1)
        else:
            try:
                open(file_to_validate, read_write).close()
            except Exception as err_validate:
                print(err_validate)
                if read_write == 'r':
                    custom_err_msg = 'Error while trying to read file'
                else:
                    custom_err_msg = 'Error while trying to write file'
                print("Not a valid file path; "+custom_err_msg)
                sys.exit(1)

    def _run(self):
        self._gen_panda_custom_df()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
