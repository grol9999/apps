import pandas
import sys
import os
import argparse


parser = argparse.ArgumentParser()

parser.add_argument("-wd",
                    type=str,
                    nargs='?',
                    help="working dir, contains the CSVs which need processing",
                    default=r'./csv_dir')

args = parser.parse_args()
default_working_directory = args.wd

if '/' != default_working_directory[-1:]:
    default_working_directory = default_working_directory+'/'
print(default_working_directory)

try:
    files_in_dir = os.listdir(default_working_directory)
except Exception as err:
    print(err)
    sys.exit(9991)

csv_filename_list = []

for file in files_in_dir:
    if 'nhtag.csv' == file[-9:]:
        os.remove(default_working_directory+file)
        continue
    if '.csv' == file[-4:]:
        csv_filename_list.append(default_working_directory+file)

print(csv_filename_list)


class CsvBeautifier:
    def __init__(self, csv_file=None, csv_file_out=None):
        self._validate_file(csv_file, 'r')
        # self._validate_file(csv_file_out, 'w')

        self.file = csv_file
        self.file_new = csv_file_out

        self.pandas_df = None
        try:
            self.pandas_df = pandas.read_csv(self.file, skiprows=[0])
        except Exception as err_reading_csv:
            print(str(err_reading_csv) + ': '+self.file + '; CSV file not in the expected format')

        self.custom_panda_df = {'Address': [], 'Content': [], 'Status Code': [], 'Indexability': [],
                                'Title 1': [], 'H1-1': [], 'H2-1': [], 'Size (bytes)': []}

        self._run()

    def _gen_panda_custom_df(self):
        extr_column_names = []
        if self.pandas_df is None:
            return False
        for df_keys in self.pandas_df.keys():
            if 'Extractor' in df_keys:
                extr_column_names.append(df_keys)

        for col_to_add in extr_column_names:
            self.custom_panda_df[col_to_add] = []
        for idx, df_row in self.pandas_df.iterrows():

            for keys_l in self.custom_panda_df.keys():
                if keys_l in df_row.keys():
                    x_dst = df_row[keys_l]
                    self.custom_panda_df[keys_l].append(x_dst)

        pandas_df_new = pandas.DataFrame(self.custom_panda_df)
        pandas_df_new.to_csv(self.file_new, index=False)

    def _validate_file(self, file_to_validate=None, read_write=''):
        if file_to_validate is None or not isinstance(file_to_validate, str):
            print("Improper usage")
            sys.exit(1)
        else:
            try:
                open(file_to_validate, read_write).close()
            except Exception as err_validate:
                print(err_validate)
                if read_write == 'r':
                    custom_err_msg = 'Error while trying to read file'
                else:
                    custom_err_msg = 'Error while trying to write file'
                print("Not a valid file path; "+custom_err_msg)
                sys.exit(1)

    def _run(self):
        self._gen_panda_custom_df()


for csv_file_name in csv_filename_list:
    f_in = csv_file_name
    f_out = csv_file_name[:-4] + 'nhtag.csv'
    CsvBeautifier(csv_file=f_in, csv_file_out=f_out)
